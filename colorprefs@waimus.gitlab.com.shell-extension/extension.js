import Gio from 'gi://Gio';
import GObject from 'gi://GObject';

import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';

import * as Main from 'resource:///org/gnome/shell/ui/main.js';

import {QuickToggle, SystemIndicator} from 'resource:///org/gnome/shell/ui/quickSettings.js';

const NightModeToggle = GObject.registerClass(
class NightModeToggle extends QuickToggle {
    _init() {
        super._init({
            title: _('Night Mode'),
            subtitle: _('Toggle UI Colors'),
            iconName: 'dark-mode-symbolic',
            toggleMode: true,
        });

        this._settings = new Gio.Settings({
            schema_id: 'org.gnome.desktop.interface',
        });
        this._changedId = this._settings.connect('changed::color-scheme',
            () => this._sync());

        this.connectObject(
            'destroy', () => this._settings.run_dispose(),
            'clicked', () => this._toggleMode(),
            this);
        this._sync();
    }

    _toggleMode() {
        Main.layoutManager.screenTransition.run();
        this._settings.set_string('color-scheme',
                                  this.checked ? 'prefer-dark' : 'default');
        // Requires adw-gtk3 theme installed
        this._settings.set_string('gtk-theme',
            this.checked ? 'adw-gtk3-dark' : 'adw-gtk3');
    }

    _sync() {
        const colorScheme = this._settings.get_string('color-scheme');
        const checked = colorScheme === 'prefer-dark';
        if (this.checked !== checked) {
            this.set({checked});
        }
        this._settings.set_string('gtk-theme',
                                  this.checked ? 'adw-gtk3-dark' : 'adw-gtk3');
        this.subtitle = this.checked ? _('Dark style active') : _('Light style active');
    }
});

export const Indicator = GObject.registerClass(
class Indicator extends SystemIndicator {
    _init() {
        super._init();

        // TODO: somehow remove the built-in dark mode toggle
        // object name is Gjs_status_darkMode_Indicator
        // Remove from quickSettingsItem

        this.quickSettingsItems.push(new NightModeToggle());
    }
});

export default class NightModeExt extends Extension {
    enable() {
        this._indicator = new Indicator(this);
        //this._indicator.quickSettingsItems.push(new NightModeToggle(this));

        Main.panel.statusArea.quickSettings.addExternalIndicator(this._indicator);
    }

    disable() {
        this._indicator.quickSettingsItems.forEach(item => item.destroy());
        this._indicator.destroy();
        this._indicator = null;
    }
}
