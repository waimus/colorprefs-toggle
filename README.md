# Global Color Preference Toggle GNOME Shell Extension

Toggle system colour preferences and includes GTK3 theme in a single button. In comparison to the default toggle button, it only changes the system color preference which will only affect supported libadwaita app.

Demo: https://mstdn.social/@waimus/109580916344986860

## Note :/
This is not very customizable. Requires specific GTK 3 theme ([adw-gtk](https://github.com/lassekongo83/adw-gtk3)) and specific gedit theme (solarized-light and merbivore, I don't remember where I got this but possible from `gedit-color-schemes` package on Fedora). I don't intend to make this to be customizable at the moment. So if anyone wants to use this, they either have to install these themes or modify the source code by themeselves.

## How it works
When toggled, it sets:
 - GSettings `org.gnome.desktop.interface` key `color-scheme` to either `prefer-dark` or `default`
 - GSettings `org.gnome.desktop.interface` key `gtk-theme` to either `adw-gtk3-dark` or `adw-gtk3`
 - GSettings `org.gnome.gedit.preferences.editor` key `scheme` to either `merbivore` or `solarized-light`

## To do, I guess
 - Make the hardcoded themes to be configurable via preferences
 - Hide the default dark mode toggle
 - Either use the same icon as the default toggle or change to sun/moon icon during toggle

# Attribution
I adapted the code from [Florian Müllner's Quick Settings Extension](https://gitlab.gnome.org/fmuellner/quick-settings-extension/-/blob/main/status/darkMode.js) and [GJS Extension documentation](https://gjs.guide/extensions/topics/quick-settings.html#basic-toggle) to figure out stuff and make this.

Additionally the base idea is inspired from [my older bash script implementation](https://gitlab.com/waimus/dotfiles/-/blob/main/desktop-home/.local/bin/colorprefs.sh) to toggle these GSettings.
 
# License
GNU General Public License version 2.0 or later.